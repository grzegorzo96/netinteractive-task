<?php

namespace App\Tests;

use App\Entity\ProgrammingLanguage;
use App\Entity\User;
use App\ValueGenerator\PasswordHashGenerator;
use PHPUnit\Framework\TestCase;

class UserCreateTest extends TestCase
{
    public function test_userCreateSuccess()
    {
        $passwordHashGenerator = new PasswordHashGenerator('zaq12wsx');
        $passwordHash = $passwordHashGenerator->generate();

        $today = new \DateTime('NOW');
        $birthdayDate = new \DateTime('16.03.1996');
        $age = ($today->diff($birthdayDate))->y;

        $active = false;

        if ($age >= 18) {
            $active = true;
        }

        $user = new User(
            "grze963@gmail.com",
            "70010112312",
            "Grzegorz",
            "Osowski",
            $birthdayDate,
            $active,
            "CLI",
            $passwordHash
        );

        $programmingLanguage = new ProgrammingLanguage("Python");
        $programmingLanguage->addProgrammer($user);

        $user->addProgrammingLanguage($programmingLanguage);

        $this->assertSame('Grzegorz', $user->getFirstName());
        $this->assertSame('Osowski', $user->getLastName());
        $this->assertSame('grze963@gmail.com', $user->getEmail());
        $this->assertSame(26, $user->getAge());
        $this->assertSame(true, $user->isActive());
        $this->assertCount(1, $user->getProgrammingLanguages());
    }

    public function test_userCreateInactive()
    {
        $passwordHashGenerator = new PasswordHashGenerator('zaq12wsx');
        $passwordHash = $passwordHashGenerator->generate();

        $today = new \DateTime('NOW');
        $birthdayDate = new \DateTime('16.03.2006');
        $age = ($today->diff($birthdayDate))->y;

        $active = false;

        if ($age >= 18) {
            $active = true;
        }

        $user = new User(
            "grze963@gmail.com",
            "70010112312",
            "Grzegorz",
            "Osowski",
            $birthdayDate,
            $active,
            "CLI",
            $passwordHash
        );

        $programmingLanguage = new ProgrammingLanguage("Python");
        $programmingLanguage->addProgrammer($user);
        $user->addProgrammingLanguage($programmingLanguage);

        $this->assertSame('Grzegorz', $user->getFirstName());
        $this->assertSame('Osowski', $user->getLastName());
        $this->assertSame('grze963@gmail.com', $user->getEmail());
        $this->assertSame(16, $user->getAge());
        $this->assertSame(false, $user->isActive());
    }

    public function test_userCreateWithWrongData()
    {
        $passwordHashGenerator = new PasswordHashGenerator('zaq12wsx');
        $passwordHash = $passwordHashGenerator->generate();

        $today = new \DateTime('NOW');
        $birthdayDate = new \DateTime('16.03.2006');
        $age = ($today->diff($birthdayDate))->y;

        $active = false;

        if ($age >= 18) {
            $active = true;
        }

        $user = new User(
            "xxx",
            "xxx",
            "Grzegorz",
            "Osowski",
            $birthdayDate,
            $active,
            "CLI",
            $passwordHash
        );

        $this->assertLessThan(11, strlen($user->getPesel()));
        $this->assertDoesNotMatchRegularExpression('/^.+\@\S+\.\S+$/', $user->getEmail());
    }
}