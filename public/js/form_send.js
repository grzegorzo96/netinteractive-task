$(document).ready(() => {
    $("#user_create").submit((e) => {
        let jsonData = {};
        $("#user_create :input").each((idx, value) => {
            if (value.id === "birthday") {
                let date = value.value.split('-')
                date = date[2] + '.' + date[1] + '.' + date[0]
                jsonData[value.id] = date;
            } else if (value.id === "programmingLanguages") {
                jsonData[value.id] = value.value.split(',')
            } else {
                jsonData[value.id] = value.value
            }
        });

        jsonData["createdFrom"] = "UI"

        $.ajax({
            type: "POST",
            url: "http://localhost:8000/api/user/create",
            dataType: 'text',
            data: JSON.stringify(jsonData),
        }).done((data) => {
            alert("Sending data is successfully")
        }).fail((jqXHR, textStatus) => {
            alert("Sending data isn't successfully")
        });

        e.preventDefault();
    })
})