function getUsers(jsonData) {
    $('#userList').html("")
    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/user/list",
        dataType: 'text',
        data: JSON.stringify(jsonData)
    }).done((data) => {
        data = $.parseJSON(data)
        data["users"].forEach((val) => {
            let programmingLanguages = ""
            val.programmingLanguages.forEach((value, idx) => {
                if (idx == (val.programmingLanguages.length - 1)) {
                    programmingLanguages += value["name"]
                } else {
                    programmingLanguages += value["name"] + ","
                }
            })

            $('#userList').append(`
                <div class="col-1">
                    ${val.id}
                </div>
                <div class="col-2">
                    ${val.email}
                </div>
                 <div class="col-1">
                    ${val.pesel}
                </div>
                 <div class="col-2">
                    ${val.firstName}
                </div>
                 <div class="col-2">
                    ${val.lastName}
                </div>
                <div class="col-1">
                    ${val.age} ${(val.ageToAdult != null) ? '/ ' + val.ageToAdult : ""}
                </div>
                <div class="col-1">
                    ${val.createdFrom}
                </div>
                 <div class="col-1">
                    ${programmingLanguages}
                </div>
                <div class="col-1">
                    ${val.active}
                </div>
            `)
        })
    }).fail((jqXHR, textStatus) => {
        alert("Failed load data")
    });
}

$(document).ready(() => {
    let jsonData = {
        createdAt: 0
    }

    $('#createdAt').change((val) => {
        jsonData["createdAt"] = val.target.value
        getUsers(jsonData)
    })

    getUsers(jsonData)

})