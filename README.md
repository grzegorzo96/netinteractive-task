# Netinteractive - task
#### Wymagania: PHP 8.1 , composer v2.4.1
1. Instalacja

```composer install```

2. Uruchomienie serwera z portem 8000

```symfony server:start --port=8000```

3. Uruchomienie testów

```./vendor/bin/phpunit```

4. Dokumentacja API

```
Lokalnie: http://127.0.0.1:8000/api/doc
Zdalnie: https://osowskicreative.pl/demo/api/doc
```

5. Wersja demo

```https://osowskicreative.pl/demo```

6. Komendy
    1. Dodawanie użytkownika

   ```bin/console admin:user:create```

    2. Aktywacja niepełnoletnich użytkowników

   ```bin/console admin:users:activeAccount```