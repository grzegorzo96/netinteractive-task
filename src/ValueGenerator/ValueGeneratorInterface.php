<?php

namespace App\ValueGenerator;

/**
 * ValueGeneratorInterface
 *
 * @author Grzegorz Osowski
 */
interface ValueGeneratorInterface
{
    /**
     * @return string|int|array|object|float|bool|null
     */
    public function generate(): string|int|array|object|float|bool|null;

}