<?php

namespace App\ValueGenerator;

/**
 * PasswordHashGenerator
 *
 * @author Grzegorz Osowski
 */
class PasswordHashGenerator implements ValueGeneratorInterface
{
    private string $plainTextPassword;

    /**
     * @param string $plainTextPassword
     */
    public function __construct(string $plainTextPassword)
    {
        $this->plainTextPassword = $plainTextPassword;
    }

    public function generate(): string
    {
        return hash("sha512", $this->plainTextPassword);
    }
}