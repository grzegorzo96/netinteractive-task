<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TwigController extends AbstractController
{
    #[Route('/')]
    public function home(): Response
    {
        return $this->render('base.html.twig',);
    }

    #[Route('/user/add')]
    public function userAdd(): Response
    {
        return $this->render('subpages/addUserForm.html.twig');
    }

    #[Route('/user/list')]
    public function userList(): Response
    {
        return $this->render('subpages/listUsers.html.twig');
    }
}