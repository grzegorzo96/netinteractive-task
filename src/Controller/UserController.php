<?php

namespace App\Controller;

use App\Entity\ProgrammingLanguage;
use App\Entity\User;
use App\Model\ProgrammingLanguageModel;
use App\Model\UserListSuccessModel;
use App\Model\UserModel;
use App\Query\UserCreateQuery;
use App\Query\UserListQuery;
use App\Repository\ProgrammingLanguageRepository;
use App\Repository\UserRepository;
use App\Service\RequestServiceInterface;
use App\Tool\ResponseTool;
use App\ValueGenerator\PasswordHashGenerator;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UserController
 *
 * @author Grzegorz Osowski <grzegorz.osowski@asuri.pl>
 */
#[OA\Tag(name: "User")]
class UserController extends AbstractController
{
    /**
     * @param Request $request
     * @param RequestServiceInterface $requestServiceInterface
     * @param UserRepository $userRepository
     * @param ProgrammingLanguageRepository $programmingLanguageRepository
     * @param MailerInterface $mailer
     * @return Response
     * @throws TransportExceptionInterface
     * @throws \Exception
     * @author Grzegorz Osowski
     */
    #[Route("/api/user/create", name: "apiUserCreate", methods: ["POST"])]
    #[OA\Post(
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: UserCreateQuery::class),
                type: "object"
            )
        ),
        responses: [
            new OA\Response(
                response: 201,
                description: "Success"
            ),
        ]
    )]
    public function userCreate(
        Request                       $request,
        RequestServiceInterface       $requestServiceInterface,
        UserRepository                $userRepository,
        ProgrammingLanguageRepository $programmingLanguageRepository,
        MailerInterface               $mailer
    ): Response
    {
        $userCreateQuery = $requestServiceInterface->getRequestBodyContent($request, UserCreateQuery::class);

        if ($userCreateQuery instanceof UserCreateQuery) {
            $user = $userRepository->findOneBy(["email" => $userCreateQuery->getEmail()]);

            if ($user != null) {
                throw new \Exception("User is exist in the system");
            }

            $passwordHashGenerator = new PasswordHashGenerator($userCreateQuery->getPassword());
            $passwordHash = $passwordHashGenerator->generate();

            $today = new \DateTime('NOW');
            $age = ($today->diff($userCreateQuery->getBirthday()))->y;

            $active = false;

            if ($age >= 18) {
                $active = true;
            }

            $user = new User(
                $userCreateQuery->getEmail(),
                $userCreateQuery->getPesel(),
                $userCreateQuery->getFirstName(),
                $userCreateQuery->getLastName(),
                $userCreateQuery->getBirthday(),
                $active,
                "API",
                $passwordHash
            );

            if($userCreateQuery->getCreatedFrom() != null){
                $user->setCreatedFrom("UI");
            }

            foreach ($userCreateQuery->getProgrammingLanguages() as $value) {
                $programmingLanguage = $programmingLanguageRepository->findOneBy(["name" => $value]);

                if ($programmingLanguage == null) {
                    $programmingLanguage = new ProgrammingLanguage($value);
                    $programmingLanguageRepository->add($programmingLanguage);
                }

                $user->addProgrammingLanguage($programmingLanguage);
            }

            $userRepository->add($user);

            if ($active) {
                $email = (new TemplatedEmail())
                    ->from('test@example.com')
                    ->to($user->getEmail())
                    ->subject('Witaj użytkowniku')
                    ->htmlTemplate('emails/createUser.html.twig');

                $mailer->send($email);
            }

            return ResponseTool::getResponse(httpCode: 201);
        } else {
            throw new \Exception("Invalid query");
        }
    }

    /**
     * @param Request $request
     * @param RequestServiceInterface $requestServiceInterface
     * @param UserRepository $userRepository
     * @return Response
     * @throws \Exception
     */
    #[Route("/api/user/list", name: "apiUserList", methods: ["POST"])]
    #[OA\Post(
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: UserListQuery::class),
                type: "object"
            ),
        ),
        responses: [
            new OA\Response(
                response: 200,
                description: "Success",
                content: new OA\JsonContent(
                    ref: new Model(type: UserListSuccessModel::class),
                    type: "object"
                )
            )
        ]
    )]
    public function userList(
        Request                 $request,
        RequestServiceInterface $requestServiceInterface,
        UserRepository          $userRepository
    ): Response
    {
        $userListQuery = $requestServiceInterface->getRequestBodyContent($request, UserListQuery::class);

        if ($userListQuery instanceof UserListQuery) {

            if ($userListQuery->getCreatedAt() != 0) {
                $now = new \DateTime('NOW');
                $now->setTime(0, 0);

                $createdThreeDaysAgo = clone $now;
                $createdThreeDaysAgo->modify('-' . $userListQuery->getCreatedAt() . ' day');

                $users = $userRepository->getListCreatedUser($createdThreeDaysAgo);
            } else {
                $users = $userRepository->findAll();
            }

            $userListSuccessModel = new UserListSuccessModel();

            foreach ($users as $value) {
                $ageToAdult = null;

                if ($value->getAge() < 18) {
                    $ageToAdult = $value->getAgeToAdult();
                }

                $userModel = new UserModel(
                    $value->getId(),
                    $value->getEmail(),
                    $value->getPesel(),
                    $value->getFirstName(),
                    $value->getLastName(),
                    $value->isActive(),
                    $value->getAge(),
                    $value->getCreatedFrom()
                );

                if ($ageToAdult != null) {
                    $userModel->setAgeToAdult($ageToAdult);
                }

                foreach ($value->getProgrammingLanguages() as $programmingLanguage) {
                    $userModel->addProgrammingLanguages(
                        new ProgrammingLanguageModel(
                            $programmingLanguage->getId(),
                            $programmingLanguage->getName()
                        )
                    );
                }

                $userListSuccessModel->addUsers($userModel);
            }

            return ResponseTool::getResponse($userListSuccessModel);
        } else {
            throw new \Exception("Invalid query");
        }
    }
}