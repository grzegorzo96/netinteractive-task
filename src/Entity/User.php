<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', unique: true)]
    private int $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private string $email;

    #[ORM\Column(type: 'string', length: 11)]
    private string $pesel;

    #[ORM\Column(type: 'string', length: 255)]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    private string $lastName;

    #[ORM\Column(type: 'boolean')]
    private bool $active;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $dateCreate;

    #[ORM\Column(type: 'date')]
    private \DateTime $birthday;

    #[ORM\Column(type: 'string', length: 10)]
    private string $createdFrom;

    #[ORM\Column(type: 'string', length: 255)]
    private string $password;

    #[ORM\ManyToMany(targetEntity: ProgrammingLanguage::class, mappedBy: 'programmer', cascade: ["persist"])]
    private Collection $programmingLanguages;

    /**
     * @param string $email
     * @param string $pesel
     * @param string $firstName
     * @param string $lastName
     * @param \DateTime $birthday
     * @param bool $active
     * @param string $createdFrom
     * @param string $password
     */
    public function __construct(string $email, string $pesel, string $firstName, string $lastName, \DateTime $birthday, bool $active, string $createdFrom, string $password)
    {
        $this->email = $email;
        $this->pesel = $pesel;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateCreate = new \DateTime('NOW');
        $this->birthday = $birthday;
        $this->active = $active;
        $this->createdFrom = $createdFrom;
        $this->password = $password;
        $this->programmingLanguages = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPesel(): string
    {
        return $this->pesel;
    }

    public function setPesel(string $pesel): self
    {
        $this->pesel = $pesel;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate(): \DateTime
    {
        return $this->dateCreate;
    }

    /**
     * @param \DateTime $dateCreate
     */
    public function setDateCreate(\DateTime $dateCreate): void
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getCreatedFrom(): string
    {
        return $this->createdFrom;
    }

    /**
     * @param string $createdFrom
     * @return $this
     */
    public function setCreatedFrom(string $createdFrom): self
    {
        $this->createdFrom = $createdFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, ProgrammingLanguage>
     */
    public function getProgrammingLanguages(): Collection
    {
        return $this->programmingLanguages;
    }

    public function addProgrammingLanguage(ProgrammingLanguage $programmingLanguage): self
    {
        if (!$this->programmingLanguages->contains($programmingLanguage)) {
            $this->programmingLanguages->add($programmingLanguage);
            $programmingLanguage->addProgrammer($this);
        }

        return $this;
    }

    public function removeProgrammingLanguage(ProgrammingLanguage $programmingLanguage): self
    {
        if ($this->programmingLanguages->removeElement($programmingLanguage)) {
            $programmingLanguage->removeProgrammer($this);
        }

        return $this;
    }

    public function getAgeToAdult(): ?string
    {
        $today = new \DateTime('NOW');
        $age = $today->diff($this->getBirthday());

        if ($age->y < 18) {
            $yearToAdult = 17 - $age->y;
            $monthsToAdult = 12 - $age->m;
            $daysToAdult = 31 - $age->d;

            return "Adult for: " . $yearToAdult . " years, " . $monthsToAdult . " months, " . $daysToAdult . " days";
        } else{
            return null;
        }
    }

    public function getAge(): int{
        $today = new \DateTime('NOW');
        return $today->diff($this->getBirthday())->y;
    }
}
