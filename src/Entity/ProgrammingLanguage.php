<?php

namespace App\Entity;

use App\Repository\ProgrammingLanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProgrammingLanguage
 *
 * @author Grzegorz Osowski <grzegorz.osowski@asuri.pl>
 */
#[ORM\Entity(repositoryClass: ProgrammingLanguageRepository::class)]
class ProgrammingLanguage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', unique: true)]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'programmingLanguages', cascade: ["persist"])]
    private Collection $programmer;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->programmer = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getProgrammer(): Collection
    {
        return $this->programmer;
    }

    public function addProgrammer(User $programmer): self
    {
        if (!$this->programmer->contains($programmer)) {
            $this->programmer->add($programmer);
        }

        return $this;
    }

    public function removeProgrammer(User $programmer): self
    {
        $this->programmer->removeElement($programmer);

        return $this;
    }
}
