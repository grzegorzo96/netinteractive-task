<?php

namespace App\Service;

use App\Serializer\JsonSerializer;
use Symfony\Component\HttpFoundation\Request;
use App\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestService implements RequestServiceInterface
{
    private ValidatorInterface $validator;

    private SerializerInterface $serializer;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->serializer = new JsonSerializer();
    }

    /**
     * @throws \Exception
     */
    public function getRequestBodyContent(Request $request, string $className): object
    {
        $bodyContent = $request->getContent();

        try {
            $query = $this->serializer->deserialize($bodyContent, $className);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        if ($query instanceof $className) {
            $validationErrors = $this->validator->validate($query);
            if ($validationErrors->count() > 0) {
                throw new \Exception($validationErrors);
            }

            return $query;
        } else {
            throw new \Exception($className);
        }
    }
}