<?php

namespace App\Model;

class UserModel
{
    /**
     * @var string
     */
    private string $id;

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $pesel;

    /**
     * @var string
     */
    private string $firstName;

    /**
     * @var string
     */
    private string $lastName;

    /**
     * @var bool
     */
    private bool $active;

    /**
     * @var int
     */
    private int $age;

    /**
     * @var string|null
     */
    private ?string $ageToAdult;

    /**
     * @var string
     */
    private string $createdFrom;

    /**
     * @var ProgrammingLanguageModel[]
     */
    private array $programmingLanguages;

    /**
     * @param string $id
     * @param string $email
     * @param string $pesel
     * @param string $firstName
     * @param string $lastName
     * @param bool $active
     * @param int $age
     * @param string $createdFrom
     */
    public function __construct(string $id, string $email, string $pesel, string $firstName, string $lastName, bool $active, int $age, string $createdFrom)
    {
        $this->id = $id;
        $this->email = $email;
        $this->pesel = $pesel;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->active = $active;
        $this->age = $age;
        $this->createdFrom = $createdFrom;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPesel(): string
    {
        return $this->pesel;
    }

    /**
     * @param string $pesel
     */
    public function setPesel(string $pesel): void
    {
        $this->pesel = $pesel;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return string|null
     */
    public function getAgeToAdult(): ?string
    {
        return $this->ageToAdult;
    }

    /**
     * @param string|null $ageToAdult
     */
    public function setAgeToAdult(?string $ageToAdult): void
    {
        $this->ageToAdult = $ageToAdult;
    }

    /**
     * @return string
     */
    public function getCreatedFrom(): string
    {
        return $this->createdFrom;
    }

    /**
     * @param string $createdFrom
     */
    public function setCreatedFrom(string $createdFrom): void
    {
        $this->createdFrom = $createdFrom;
    }

    /**
     * @return ProgrammingLanguageModel[]
     */
    public function getProgrammingLanguages(): array
    {
        return $this->programmingLanguages;
    }

    /**
     * @param ProgrammingLanguageModel[] $programmingLanguages
     */
    public function setProgrammingLanguages(array $programmingLanguages): void
    {
        $this->programmingLanguages = $programmingLanguages;
    }

    public function addProgrammingLanguages(ProgrammingLanguageModel $programmingLanguageModel): void
    {
        $this->programmingLanguages[] = $programmingLanguageModel;
    }
}