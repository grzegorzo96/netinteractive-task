<?php

namespace App\Model;

class UserListSuccessModel implements ModelInterface
{
    /**
     * @var UserModel[]
     */
    private array $users;

    /**
     * @return UserModel[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param UserModel[] $users
     */
    public function setUsers(array $users): void
    {
        $this->users = $users;
    }

    public function addUsers(UserModel $userModel)
    {
        $this->users[] = $userModel;
    }
}