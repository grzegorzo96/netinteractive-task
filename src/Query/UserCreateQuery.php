<?php

namespace App\Query;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * UserCreateQuery
 *
 * @author Grzegorz Osowski
 */
class UserCreateQuery
{
    #[Assert\NotNull(message: "Email is null")]
    #[Assert\NotBlank(message: "Email is blank")]
    #[Assert\Type(type: "string")]
    private string $email;

    #[Assert\NotNull(message: "FirstName is null")]
    #[Assert\NotBlank(message: "FirstName is blank")]
    #[Assert\Type(type: "string")]
    private string $firstName;

    #[Assert\NotNull(message: "LastName is null")]
    #[Assert\NotBlank(message: "LastName is blank")]
    #[Assert\Type(type: "string")]
    private string $lastName;

    #[Assert\NotNull(message: "Pesel is null")]
    #[Assert\NotBlank(message: "Pesel is blank")]
    #[Assert\Length(min: 11, max: 11)]
    #[Assert\Type(type: "string")]
    private string $pesel;

    #[Assert\NotNull(message: "Birthday is null")]
    #[Assert\NotBlank(message: "Birthday is blank")]
    #[Assert\Type(type: "datetime")]
    private DateTime $birthday;

    #[Assert\NotNull(message: "Password is null")]
    #[Assert\NotBlank(message: "Password is blank")]
    #[Assert\Type(type: "string")]
    private string $password;

    #[Assert\Type(type: "string")]
    private ?string $createdFrom;

    #[Assert\NotNull(message: "ProgrammingLanguages is null")]
    #[Assert\NotBlank(message: "ProgrammingLanguages is blank")]
    #[Assert\All(constraints: [
        new Assert\Type(type: "string")
    ])]
    private array $programmingLanguages;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    #[OA\Property(type: "string", example: "70010112312")]
    public function getPesel(): string
    {
        return $this->pesel;
    }

    /**
     * @param string $pesel
     */
    public function setPesel(string $pesel): void
    {
        $this->pesel = $pesel;
    }

    /**
     * @return DateTime
     */
    #[OA\Property(property: "birthday", example: "d.m.Y")]
    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     */
    public function setBirthday(string $birthday): void
    {
        $this->birthday = DateTime::createFromFormat('d.m.Y', $birthday);;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string|null
     */
    public function getCreatedFrom(): ?string
    {
        return $this->createdFrom;
    }

    /**
     * @param string|null $createdFrom
     */
    public function setCreatedFrom(?string $createdFrom): void
    {
        $this->createdFrom = $createdFrom;
    }

    /**
     * @return string[]
     */
    public function getProgrammingLanguages(): array
    {
        return $this->programmingLanguages;
    }

    /**
     * @param string[] $programmingLanguages
     */
    public function setProgrammingLanguages(array $programmingLanguages): void
    {
        $this->programmingLanguages = $programmingLanguages;
    }
}