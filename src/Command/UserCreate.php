<?php

namespace App\Command;

use App\Entity\ProgrammingLanguage;
use App\Entity\User;
use App\Repository\ProgrammingLanguageRepository;
use App\Repository\UserRepository;
use App\ValueGenerator\PasswordHashGenerator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

/**
 * UserCreate
 *
 * @author Grzegorz Osowski
 */
#[AsCommand(
    name: 'admin:user:create',
    description: "Administrator of application create user in the system."
)]
class UserCreate extends Command
{
    private UserRepository $userRepository;

    private ProgrammingLanguageRepository $programmingLanguageRepository;

    private MailerInterface $mailer;

    /**
     * @param UserRepository $userRepository
     * @param ProgrammingLanguageRepository $programmingLanguageRepository
     * @param MailerInterface $mailer
     */
    public function __construct(UserRepository $userRepository, ProgrammingLanguageRepository $programmingLanguageRepository, MailerInterface $mailer)
    {
        $this->userRepository = $userRepository;
        $this->programmingLanguageRepository = $programmingLanguageRepository;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('firstName', InputArgument::REQUIRED, 'User firstName');
        $this->addArgument('lastName', InputArgument::REQUIRED, 'User lastName');
        $this->addArgument('email', InputArgument::REQUIRED, 'User email');
        $this->addArgument('password', InputArgument::REQUIRED, 'User password');
        $this->addArgument('birthday', InputArgument::REQUIRED, 'User birthday e.g. 01-01-1970');
        $this->addArgument('pesel', InputArgument::REQUIRED, 'User PESEL number e.g. 70010112312');
        $this->addArgument('programmingLanguages', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'User programming language e.g. example Python PHP JavaScript');
    }

    /**
     * @throws \Exception
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $firstName = $input->getArgument('firstName');
        $lastName = $input->getArgument('lastName');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $birthday = $input->getArgument('birthday');
        $pesel = $input->getArgument('pesel');
        $programmingLanguages = $input->getArgument('programmingLanguages');

        $passwordHashGenerator = new PasswordHashGenerator($password);
        $passwordHash = $passwordHashGenerator->generate();

        $today = new \DateTime('NOW');
        $birthday = new \DateTime($birthday);
        $age = ($today->diff($birthday))->y;

        $active = false;

        if ($age >= 18) {
            $active = true;
        }

        $user = new User(
            $email,
            $pesel,
            $firstName,
            $lastName,
            $birthday,
            $active,
            "CLI",
            $passwordHash
        );

        foreach ($programmingLanguages as $value) {
            $programmingLanguage = $this->programmingLanguageRepository->findOneBy(["name" => $value]);

            if ($programmingLanguage == null) {
                $programmingLanguage = new ProgrammingLanguage($value);
                $this->programmingLanguageRepository->add($programmingLanguage);
            }

            $user->addProgrammingLanguage($programmingLanguage);
        }

        $this->userRepository->add($user);

        if ($active) {
            $email = (new TemplatedEmail())
                ->from('grze963testowy@gmail.com')
                ->to($user->getEmail())
                ->subject('Witaj użytkowniku')
                ->htmlTemplate('emails/createUser.html.twig');

            $this->mailer->send($email);
        }

        $io->success('Success of creating user.');

        return Command::SUCCESS;
    }
}