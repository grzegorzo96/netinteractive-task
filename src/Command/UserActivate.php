<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

#[AsCommand(
    name: 'admin:users:activeAccount',
    description: "Administrator activate account for users 18 years old "
)]
class UserActivate extends Command
{
    private UserRepository $userRepository;

    private MailerInterface $mailer;

    /**
     * @param UserRepository $userRepository
     * @param MailerInterface $mailer
     */
    public function __construct(UserRepository $userRepository, MailerInterface $mailer)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;

        parent::__construct();
    }

    /**
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $users = $this->userRepository->findBy(["active" => false]);

        foreach ($users as $user) {
            if ($user->getAge() >= 18) {
                $user->setActive(true);
                $this->userRepository->add($user);

                $email = (new TemplatedEmail())
                    ->from('grze963testowy@gmail.com')
                    ->to($user->getEmail())
                    ->subject('Witaj użytkowniku')
                    ->htmlTemplate('emails/activateUser.html.twig');

                $this->mailer->send($email);
            }
        }

        $io->success('Success of activate users.');

        return Command::SUCCESS;
    }
}