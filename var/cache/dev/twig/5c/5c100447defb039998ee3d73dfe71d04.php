<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* subpages/addUserForm.html.twig */
class __TwigTemplate_41b906fee7b4237e4995937f8f958eed extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "subpages/addUserForm.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "subpages/addUserForm.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "subpages/addUserForm.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-2\"></div>
        <div class=\"col-8\">
            <form id=\"user_create\" method=\"POST\">
                <div class=\"form-group\">
                    <label>Email address</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter email\">
                </div>
                <div class=\"form-group\">
                    <label>Password</label>
                    <input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Enter passsword\">
                </div>
                <div class=\"form-group\">
                    <label>Pesel</label>
                    <input type=\"text\" class=\"form-control\" id=\"pesel\"
                           onkeyup=\"this.value=this.value.replace(/[^\\d]/,'')\"
                           onkeydown=\"return ( event.ctrlKey || event.altKey
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false)
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9)
                                    || (event.keyCode>34 && event.keyCode<40)
                                    || (event.keyCode==46) )\"
                           maxlength=\"11\"
                           placeholder=\"Enter PESEL number\">
                </div>
                <div class=\"form-group\">
                    <label>First name</label>
                    <input type=\"text\" class=\"form-control\" id=\"firstName\" placeholder=\"Enter first name\">
                </div>
                <div class=\"form-group\">
                    <label>Last name</label>
                    <input type=\"text\" class=\"form-control\" id=\"lastName\" placeholder=\"Enter last name\">
                </div>
                <div class=\"form-group\">
                    <label>Birthday</label>
                    <input type=\"date\" class=\"form-control\" id=\"birthday\">
                </div>
                <div class=\"form-group\">
                    <label>Progamming language</label>
                    <input type=\"text\" class=\"form-control\" id=\"programmingLanguages\" placeholder=\"Enter programming languages e.g. Python,Java,C++\">
                </div>
                <button type=\"submit\" class=\"btn btn-primary\">Create user</button>
                <a href=\"/\" class=\"btn btn-primary\">Back</a>
            </form>
        </div>
        <div class=\"col-2\"></div>
    </div>

    <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/form_send.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "subpages/addUserForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 52,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-2\"></div>
        <div class=\"col-8\">
            <form id=\"user_create\" method=\"POST\">
                <div class=\"form-group\">
                    <label>Email address</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter email\">
                </div>
                <div class=\"form-group\">
                    <label>Password</label>
                    <input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Enter passsword\">
                </div>
                <div class=\"form-group\">
                    <label>Pesel</label>
                    <input type=\"text\" class=\"form-control\" id=\"pesel\"
                           onkeyup=\"this.value=this.value.replace(/[^\\d]/,'')\"
                           onkeydown=\"return ( event.ctrlKey || event.altKey
                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false)
                                    || (95<event.keyCode && event.keyCode<106)
                                    || (event.keyCode==8) || (event.keyCode==9)
                                    || (event.keyCode>34 && event.keyCode<40)
                                    || (event.keyCode==46) )\"
                           maxlength=\"11\"
                           placeholder=\"Enter PESEL number\">
                </div>
                <div class=\"form-group\">
                    <label>First name</label>
                    <input type=\"text\" class=\"form-control\" id=\"firstName\" placeholder=\"Enter first name\">
                </div>
                <div class=\"form-group\">
                    <label>Last name</label>
                    <input type=\"text\" class=\"form-control\" id=\"lastName\" placeholder=\"Enter last name\">
                </div>
                <div class=\"form-group\">
                    <label>Birthday</label>
                    <input type=\"date\" class=\"form-control\" id=\"birthday\">
                </div>
                <div class=\"form-group\">
                    <label>Progamming language</label>
                    <input type=\"text\" class=\"form-control\" id=\"programmingLanguages\" placeholder=\"Enter programming languages e.g. Python,Java,C++\">
                </div>
                <button type=\"submit\" class=\"btn btn-primary\">Create user</button>
                <a href=\"/\" class=\"btn btn-primary\">Back</a>
            </form>
        </div>
        <div class=\"col-2\"></div>
    </div>

    <script src=\"{{ asset('js/form_send.js') }}\"></script>
{% endblock %}", "subpages/addUserForm.html.twig", "/Users/grzegorzosowski/osowskicreative/netinteractive_zadanie/templates/subpages/addUserForm.html.twig");
    }
}
