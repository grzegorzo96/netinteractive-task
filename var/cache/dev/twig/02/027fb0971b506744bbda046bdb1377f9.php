<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* subpages/listUsers.html.twig */
class __TwigTemplate_733c8696193000c530dd47d39c0fc33f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "subpages/listUsers.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "subpages/listUsers.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "subpages/listUsers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"row\">
                <div class=\"col-1\">
                    Id
                </div>
                <div class=\"col-2\">
                    Email
                </div>
                <div class=\"col-1\">
                    Pesel
                </div>
                <div class=\"col-2\">
                    FirstName
                </div>
                <div class=\"col-2\">
                    LastName
                </div>
                <div class=\"col-1\">
                    Age
                </div>
                <div class=\"col-1\">
                    CreatedFrom
                </div>
                <div class=\"col-1\">
                    Programming Languages
                </div>
                <div class=\"col-1\">
                    Active
                </div>
            </div>
            <div class=\"row\" id=\"userList\"></div>
            <div class=\"row\">
                <div class=\"col-12\">
                    <label for=\"createdAt\">Choose day created user</label>
                    <select id=\"createdAt\">
                        <option value=\"0\">All users</option>
                        <option value=\"3\">3 days ago</option>
                        <option value=\"7\">7 days ago</option>
                        <option value=\"30\">30 days ago</option>
                    </select>
                </div>
                <div class=\"col-12\">
                    <a href=\"/\" class=\"btn btn-primary\">Back</a>
                </div>
            </div>
        </div>
    </div>

    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/list_user.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "subpages/listUsers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 53,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-12\">
            <div class=\"row\">
                <div class=\"col-1\">
                    Id
                </div>
                <div class=\"col-2\">
                    Email
                </div>
                <div class=\"col-1\">
                    Pesel
                </div>
                <div class=\"col-2\">
                    FirstName
                </div>
                <div class=\"col-2\">
                    LastName
                </div>
                <div class=\"col-1\">
                    Age
                </div>
                <div class=\"col-1\">
                    CreatedFrom
                </div>
                <div class=\"col-1\">
                    Programming Languages
                </div>
                <div class=\"col-1\">
                    Active
                </div>
            </div>
            <div class=\"row\" id=\"userList\"></div>
            <div class=\"row\">
                <div class=\"col-12\">
                    <label for=\"createdAt\">Choose day created user</label>
                    <select id=\"createdAt\">
                        <option value=\"0\">All users</option>
                        <option value=\"3\">3 days ago</option>
                        <option value=\"7\">7 days ago</option>
                        <option value=\"30\">30 days ago</option>
                    </select>
                </div>
                <div class=\"col-12\">
                    <a href=\"/\" class=\"btn btn-primary\">Back</a>
                </div>
            </div>
        </div>
    </div>

    <script src=\"{{ asset('js/list_user.js') }}\"></script>
{% endblock %}", "subpages/listUsers.html.twig", "/Users/grzegorzosowski/osowskicreative/netinteractive_zadanie/templates/subpages/listUsers.html.twig");
    }
}
